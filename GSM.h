#ifndef GSM_H
#define GSM_H

#include "Arduino.h"
#include <SoftwareSerial.h>

//////////////
// GSM NODE //
//////////////
enum _TCP_STATES {
  TCP_CONNECTED,
  TCP_DISCONNECTED
};

static uint8_t TCP_STATE          = TCP_DISCONNECTED;
static unsigned long   _timeSent  = 0;

static byte batteryLevelUpdated   = false;
static char * batteryLevel        = new char[16];
  
class GSMNode {
  friend class GSMLinkedList;
  friend class GSMUtils;
  
  public:
    GSMNode(char cmd[], char resp[], char error[], unsigned long timeOut) : 
      cmd(cmd), error(error), resp(resp), _timeOut(timeOut), next(NULL),
      prev(NULL), state(0), rollBackCounter(0),
      retryCounter(1), temp(0), set(false), ATSent(false) {};
    
    virtual void sendATcommand(SoftwareSerial * GSMPort);
    virtual void parseATcommand(const char * input);

    void clearNode();
    bool empty();
    
  protected:
    GSMNode *       next;
    GSMNode *       prev;

    char            *cmd;    // Cmd to be sent
    char            *resp;   // Expected response
    char            *error;  // Alternate error message
    unsigned long   _timeOut;
    uint8_t         retryCounter;
    uint8_t         state;
    uint8_t         rollBackCounter;
    byte            temp;
    byte            set;     // Identifies whether the execution node has been set or not
    byte            ATSent;  // Flag to identify whether this nodes CMD has been sent (to stop using responses from previous cmd's as it's response)

    void reset();
};
/////////////////////
// GSM STATUS NODE //
/////////////////////
class GSMStatusNode : public GSMNode {
  public:
    GSMStatusNode(char cmd[], char resp[], unsigned long timeOut) : GSMNode(cmd, resp, "ERROR", timeOut) {};
  
    virtual void sendATcommand(SoftwareSerial * GSMPort);
    virtual void parseATcommand(const char * input);
};
/////////////////////
// GSM LINKED LIST //
/////////////////////
class GSMLinkedList {
  friend class GSMUtils;
  
  private:
    GSMNode *      head;
    GSMNode *      tail;
    uint8_t        maxLength;
    
    GSMLinkedList() : head(NULL), tail(NULL), maxLength(20) {};
    // Add a new node to the end of the linked list //
    void tailAppend(GSMNode * node);
    // Gets the first available node //
    GSMNode * getFree();
    // Reset list //
    void reset();
    // Destroys a list //
    void destroy();
    // Moves front of the execution queue to the back //
    void processHead();
};

///////////////////
// GSM UTILITIES //
///////////////////

class GSMUtils {
  friend class GSMStatusNode;
  
  public:
    SoftwareSerial *  GSMPort;
  
  private:
    GSMNode *         currentNode;
    GSMNode *         currentExecNode;
    GSMLinkedList     GSMStack;
    GSMLinkedList     GSMExecutionQueue;

    byte              resetArduino;
    
    uint8_t           RX;
    uint8_t           TX;
    uint8_t           RST;

    char              c;
    char              input[50];
    const char        serverMessageFlag[3] = { '.',':','\0' };
    const char        *_ID; // Tracker ID
    
    // Latch Variables
    byte              latchToggle;
    unsigned long     latchStartTime;
    unsigned long     latchTimeOut;
    uint8_t           latchPin;

    // Battery Variables
    unsigned long     batteryReadTimeOut;
    unsigned long     lastBatteryRead;

    // Misc. function
    void printList();
    
  public:
    GSMUtils(const char ip[], const char port[], uint8_t RX, uint8_t TX, uint8_t RST) : RX(RX), TX(TX), RST(RST), latchToggle(false), latchStartTime(0), latchTimeOut(0), latchPin(-1), lastBatteryRead(0), resetArduino(false) {
      GSMPort      = new SoftwareSerial(RX, TX);
  
      // INITIATE TCP CONNECTION STACK //
      //GSMStack.tailAppend(new GSMNode("AT",                                                   "OK",         "ERROR",        3000)); // Initiall AT check
      GSMStack.tailAppend(new GSMStatusNode("AT+CIPSTATUS",                                   "CONNECT OK",                 5000));   // Get connection status
      GSMStack.tailAppend(new GSMNode("AT+CIPSHUT",                                           "SHUT OK",    "ERROR",        5000));   // Cancel exisiting TCP connections
      GSMStack.tailAppend(new GSMNode("AT+CIPMUX=0",                                          "OK",         "ERROR",        3000));   // Single connection mode
      GSMStack.tailAppend(new GSMNode("AT+CGATT=1",                                           "OK",         "ERROR",        3000));   // Attach to GPRS
      GSMStack.tailAppend(new GSMNode("AT+CSTT=\"www\",\"\",\"\"",                            "OK",         "ERROR",        8000));   // Initiate connection
      GSMStack.tailAppend(new GSMNode("AT+CIICR",                                             "OK",         "ERROR",        8000));   // Bring up GPRS connection
      GSMStack.tailAppend(new GSMNode("AT+CIFSR",                                             ".",          "TCP CLOSED",   8000));   // Check local IP
      
      // Generate CIPSTART string //
      char * cipstart = new char[50]; memset(cipstart, 0, 50);
      strcat(cipstart, "AT+CIPSTART=\"TCP\",\"");
      strcat(cipstart, ip);
      strcat(cipstart, "\",\"");
      strcat(cipstart, port);
      strcat(cipstart, "\"");
      GSMStack.tailAppend(new GSMNode(cipstart,                                               "CONNECT OK", "CONNECT FAIL", 20000));  // Connect
      
      // INITIATE TCP EXECUTION QUEUE //
      GSMExecutionQueue.maxLength = 3;
      for (int i = 0; i < GSMExecutionQueue.maxLength; i++) {
        GSMExecutionQueue.tailAppend(new GSMNode(new char[70], new char[12], new char[12], 0));
        GSMExecutionQueue.tail->prev = GSMStack.head;
        GSMExecutionQueue.tail->clearNode(); // initial setup of allocated memory
        GSMExecutionQueue.tail->temp = true; // identifier for execution nodes
      }
      GSMExecutionQueue.tail->next = GSMStack.head;
      
      // SET GSM RST PIN TO HIGH //
      pinMode(RST, OUTPUT);
      digitalWrite(RST, HIGH);
      
      // BEGIN COMMUNICATION ON GSM PORT //
      GSMPort->begin(9600);

      // SET THE CURRENT NODE POINTER //
      currentNode  = GSMStack.head;
#ifdef __DEBUG__
      Serial.println(F("** Successfully setup GSM **"));
#endif
    };
    
    // Update methods
    void update_TCP_connection();
    void update_battery_level();
    // Status methods
    boolean connected();
    // Getters / Setters
    SoftwareSerial * getGSMPort() { return GSMPort; }
    void setTrackerID(const char * _ID) { this->_ID = _ID; };
    void setBatteryReadInterval(unsigned long batteryReadTimeOut) { this->batteryReadTimeOut = batteryReadTimeOut; };
    byte batteryLevelIsUpdated();
    char * getBatteryLevel();
    void setBatteryLevelUpdated(byte batteryLevelUpdateStatus);
    byte getArduinoReset();
    // Misc.
    void sendATcommand(GSMNode * cmd, const char * ID);
    boolean QNF(uint8_t amount);
    boolean sendPacket(char * pktIn);
    void reset();
};
#endif
