//#define __DEBUG__ // Used for debugging purposes
#include <SoftwareSerial.h>
#include <avr/pgmspace.h>

#ifdef __DEBUG__
#include "MemoryFree.h" 
SoftwareSerial Logger(3,4);
uint8_t state = 0;
#endif

#define GPSPort Serial

#include "GPS.h"
#include "GSM.h"



// SERVER PACKET VARIALBES //
const char    *_ID                           = "1",
              *serverIP                      = "tracker.thanas.co.za",
              *serverPort                    = "40713";
unsigned long _timeSincePacketSent           = 0;
char          *packet; // Packet being sent

// MISC. VARIABLES //
// Write
const uint8_t testLight                     = 9,
              // Read
              alarmStatePin                 = 7;
const unsigned long ULONG_MAX               = 4294967295;

// UPDATE TIMES //
const unsigned long alarmStateUpdate        = 5000,   // How often the alarm state is read
                    packetSendUpdate        = 1000,   // How often a packet is sent to the server
                    batteryLevelUpdate      = 30000; // How often the battery level is read (via GSM module)=
unsigned long       timeSinceLastPacketSent = 0;      // Time since the last packet has been sent

// TCP CONNECTION //
GSMUtils * TCPConnection;

#include "Alarm.h"


void setup() {
  Serial.begin(9600);
  
  delay(1000);
  
  // Setup GPS
  GPS::initialise_GPS();
  // Setup GSM
  TCPConnection = new GSMUtils(serverIP, serverPort, 10, 11, 2); // RX, TX, RST
  TCPConnection->setTrackerID(_ID);
  TCPConnection->setBatteryReadInterval(batteryLevelUpdate);
  // Set defaults on pins out
  pinMode(testLight, OUTPUT);
  digitalWrite(testLight, HIGH);
  // Allocate space for packet
  packet = new char[70];
  
#ifdef __DEBUG__
  Logger.begin(9600);
#endif
}

void loop() {
#ifdef __DEBUG__
  TCPConnection->GSMPort->listen();  
#endif
  // Check for arduino to be reset //
  if (TCPConnection->getArduinoReset()) resetArduino();

  // UPDATE TCP CONNECTION INFO //
  TCPConnection->update_TCP_connection();

  // UPDATE ALARM STATUS //
  Alarm::update_alarm_state();

  // UPDATE GPS //
  GPS::process_GPS();

  // SEND TCP PACKET //
  if (millis() - timeSinceLastPacketSent > packetSendUpdate) {
#ifdef __DEBUG__
    Logger.println(freeMemory());
#endif
    if (TCPConnection->QNF(2)) {
      TCPConnection->sendPacket(buildPacket());
    }
    timeSinceLastPacketSent = millis();
    //digitalWrite(9, !digitalRead(9));
  }
}

/*
   Method used to create a packet which is then sent to the server
   ------------------------------------------------------------------
   @OUTPUT c-string containing a packet with GPS coordinates, battery level and alarm state
*/
char * buildPacket() {
  // Clear memory in that address space
  memset(packet, 0, 70);
  // Append positional DATA
  if (GPS::updated) { // Don't waste data sending invalid packets when there are no satellites
    if (GPS::nav.hAcc != ULONG_MAX) {
      strcat(packet, String(GPS::nav.lat).c_str()); strcat(packet, ",");
      strcat(packet, String(GPS::nav.lon).c_str()); strcat(packet, ",");
      strcat(packet, String(GPS::nav.hAcc / 1000.0f).c_str()); strcat(packet, ",");
      strcat(packet, String(GPS::nav.numSV).c_str()); strcat(packet, ",");
      strcat(packet, String(GPS::nav.gSpeed / 1000.0f).c_str()); strcat(packet, ",");
      strcat(packet, String(GPS::nav.heading / 100000.0f).c_str()); strcat(packet, ",");
    } else strcat(packet, "-1,-1,-1,-1,-1,-1,");
    GPS::updated = false;
  } else {
    strcat(packet, ",,,,,,");  // Ensure GPS connection is kept alive(
    GPS::updateAttempts++;
  }

  // Append battery level
  if (TCPConnection->batteryLevelIsUpdated()) {
    strncat(packet, TCPConnection->getBatteryLevel(), 4);
    TCPConnection->setBatteryLevelUpdated(false);
  } strcat(packet, ",");

  // Append alarm state
  if (Alarm::alarmUpdated) {
    strcat(packet, Alarm::alarmState);
    Alarm::alarmUpdated = false;
  } strcat(packet, ",");
  return packet;
}

/*
   Method used to do a softrest of the arduino
*/
void resetArduino() {
  asm volatile ("  jmp 0");
}

