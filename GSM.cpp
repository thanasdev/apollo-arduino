#define MAX_NODE_RETRIES 3      // Maximum number of retries a GSMNode can make
#define MAX_NODE_ROLLBACK 3     // Maximum amount of times a node can be 'rolled back onto' before throwing an error
#define MAX_TOP_NODE_ROLLBACK 5 // Maximum roll back counter at the head node of the GSMStack before throwing an error

//#define __DEBUG__

#include "GSM.h"

const char * _error       = "ERROR";
const char * _exception   = "OK";
const char * _delimeter   = ":";

const uint8_t _reserved_pins[] = { 10, 11, 2, 7 }; // Pin values which are used for Serial data / RST / pinMode(INPUT)

//////////////
// GSM NODE //
//////////////
void GSMNode::sendATcommand(SoftwareSerial * GSMPort) {
  if ((millis() - _timeSent) > _timeOut || retryCounter == 1) {
#ifdef __DEBUG__
    if (retryCounter == 1) {
      Serial.print(F("Sending AT command: "));Serial.println(cmd);
    }
    Serial.print(F("Attempt ")); Serial.print(retryCounter); Serial.println(F("..."));
#endif
    // Send AT command //
    GSMPort->println(cmd);
    // Set sent flag to true //
    ATSent = true;
    // Increment counter //
    retryCounter++;
    // Update sent time //
    _timeSent = millis();
  }
}
/*
 * Method used to parse responses from the GSM Module
 * -----------------------------------------------------
 * The state of the currentNode is set based on the response expected from the node.
 * @PARAM input is a null terminated c string which contains the input
 *  from the GSM module
 */
void GSMNode::parseATcommand(const char * input) {
  if (strcmp(cmd, "AT+CBC") == 0) { // Battery Update
    if (strstr(input, resp) != NULL) { // +CBC ...
      strcpy(batteryLevel, input);
      for (int i = 0; i < 4; i++)
        *(batteryLevel + i) = *(batteryLevel + i + 11);
      *(batteryLevel + 4) = '\0';
      state = 1;
      batteryLevelUpdated = true;
    } else if (strstr(input, error) != NULL) { // ERROR //
      state = 2; retryCounter++;
    }
  } else {
    if (strstr(input, resp) != NULL) {
      state = 1; retryCounter = 1;
    }
    // check if the desired answer 2 is in the response of the module
    else if ((strstr(input, error) != NULL) || (strstr(input, ::_error) != NULL))
      state = 2;
  }
#ifdef __DEBUG__
  Serial.print(F("Response: ")); Serial.print(input); Serial.print(F(" (")); Serial.print(state); Serial.println(F(")"));
#endif
}

void GSMNode::reset() {
  state = 0;
  retryCounter = 1;
  ATSent = false;
}

/*
 * Method used to check if an execution node has had it's CMD set
 */
boolean GSMNode::empty() {
  return !set;
}

/*
 * Method used to reset the nodes commands
 * (Only used by execution nodes)
 */
void GSMNode::clearNode() {
  memset(error, 0, 12);
  memset(resp, 0, 12);
  memset(cmd, 0, 70);
  set = false;
  reset();
}

/////////////////////
// GSM STATUS NODE //
/////////////////////
void GSMStatusNode::sendATcommand(SoftwareSerial * GSMPort) {
  if (millis() - _timeSent > _timeOut) {
#ifdef __DEBUG__
    if (retryCounter == 1) {
      Serial.print(F("Sending AT status command: "));Serial.println(cmd);
    } else {
      Serial.print(F("Attempt "));Serial.print(retryCounter);Serial.println(F("..."));
    }
#endif
    // Send AT command //
    GSMPort->println(cmd);
    // Set sent flag to true //
    ATSent = true;
    // Update sent time //
    _timeSent = millis();
    // Check for GSMStatusNode error state (3)
    if (state == 3)
      retryCounter++;
  }
}

void GSMStatusNode::parseATcommand(const char * input) {
  if (strstr(input, resp) != NULL) { // CONNECT OK //
    state = 2;
    // Signal TCP is connected //
    TCP_STATE = TCP_CONNECTED;
  } else if (strstr(input, error) != NULL) { // ERROR //
    state = 2; retryCounter++;
    // Signal TCP disconnect //
    TCP_STATE = TCP_DISCONNECTED;
  } else if ((strstr(input, ::_exception) == NULL) && strlen(input) > 1) { // Anything other than OK and ERROR //
    state = 1; retryCounter = 1;
    // Signal TCP disconnect //
    TCP_STATE = TCP_DISCONNECTED;
  } else {
    state = 3;
  }
#ifdef __DEBUG__
  Serial.print(F("Response: ")); Serial.print(input); Serial.print(F(" (")); Serial.print(state); Serial.println(F(")"));
#endif
}

///////////////////
// GSM NODE LIST //
///////////////////

void GSMLinkedList::tailAppend(GSMNode * node) {
  if (head == NULL && tail == NULL)
    head = tail = node;
  else {
    tail->next = node;
    node->prev = tail;
    tail = node;
  }
}

/*
 * Method used to return the first free node on the execution queue
 * -------------------------------------------------------------
 * @OUTPUT GSMNode pointer to the first free node
 */
GSMNode * GSMLinkedList::getFree() {
  GSMNode   *tmp = head;
  uint8_t   _index = 10; // Failsafe index
  while (tmp != NULL && tmp->temp && _index-- > 0) {
    if (tmp->empty()) break;
    else tmp = tmp->next;
  }
  return tmp;
}

/*
 * Method used to change the state and retry counter of all nodes
 */
void GSMLinkedList::reset() {
  uint8_t   _index = 10; // Fail safe index
  GSMNode   *tmp = head;
  
  while (tmp != NULL && _index-- > 0) {
    tmp->reset();
    tmp = tmp->next;
  }
}

// Set all the exec queue nodes to false, to ensure
// there are no 'hanging commads'(ones where the current
// AT command relies on the previous one, such as with sending
// a TCP packet)
void GSMLinkedList::destroy() {
#ifdef __DEBUG__
  Serial.println(F("** Destroying Queue **"));
#endif
  GSMNode   *tmp = head;
  uint8_t   _index = 10; // Failsafe index
  while(tmp != NULL && tmp->temp && _index-- > 0) {
    tmp->clearNode();
    tmp = tmp->next;
  }
}

/*
 * Method used to shift the front of the executionQueue to the back
 * ---------------------------------------------------------------
 * The head of the executionQueue is "moved" to the back of the queue, by rearranging 
 * pointers. The node which is moved to the back of the queue is completely reset.
 */
void GSMLinkedList::processHead() {
  GSMNode * tmp = head;

  head = head->next;
  tmp->next = tmp->prev;
  tail->next = tmp;
  tail = tmp;
  // Allow for the node to be overwritten
  tmp->set = false;
  // Reset
  tmp->reset();
  tmp->clearNode();
}

///////////////////
// GSM UTILITIES //
///////////////////

boolean GSMUtils::connected() {
  if (TCP_STATE == TCP_CONNECTED)
    return true;
  return false;
}

void GSMUtils::update_TCP_connection() {
  // RESET GSM STACK //
  // For the first time a connection is established (when AT+CIPSTART->state == 1),
  // reset the connection queue to ensure that the connection is kept alive by a constant
  // polling.
  if (GSMStack.tail->state == 1) {
    GSMStack.reset();             // Reset the state of the reconnection queue
    GSMExecutionQueue.destroy();  // Ensure only most recent packets are sent
    currentNode = GSMStack.head;  // Go back to the start of the queue
  } // END OF RESET (1)

  // PARSE GSM INPUT //
  // Capture the input in a buffer and handle it accordingly
  // by passing it to the relevant node's parse method, or 
  // if it's a server message (.:), handle the response in this method
  if (GSMPort->available() != 0) {
    c = GSMPort->read();  // Read the incoming char
    if (c == '\n' || (strstr(input, ">") != NULL)) { // End of serial input OR awaiting TCP data //
      strncat(input, '\0', 1);  // Ensure the string is null terminated
      // --------------------
      // -- SERVER MESSAGE --
      // --------------------
      if (false) {//strstr(input, serverMessageFlag) != NULL) {
#ifdef __DEBUG__
        Serial.print(F("SERVER MESSAGE: ")); Serial.println(input);
#endif
        // AT COMMAND (.:AT:<cmd>:<resp>:<error>:<timeout>:<ACK ID>) //
        // ** RESP: ACK,<ACK ID>
        /*if (strstr(input, ".:AT:") != NULL)  {
          String        cmd, resp, error, ACK_ID;
          unsigned long timeout;
          uint8_t       i1, i2, iTemp;
          i1 = input.indexOf(_delimeter,2);i2 = input.indexOf(_delimeter, i1+1);iTemp = i1;cmd = input.substring(i1+1, i2);                   // CMD
          i1 = input.indexOf(_delimeter,iTemp+1);i2 = input.indexOf(_delimeter,i1+1);iTemp = i1;resp = input.substring(i1+1, i2);             // RESP
          i1 = input.indexOf(_delimeter,iTemp+1);i2 = input.indexOf(_delimeter,i1+1);iTemp = i1;error = input.substring(i1+1, i2);            // ERROR
          i1 = input.indexOf(_delimeter,iTemp+1);i2 = input.indexOf(_delimeter,i1+1);iTemp = i1;timeout = input.substring(i1+1, i2).toInt();  // TIMEOUT
          i1 = input.indexOf(_delimeter,iTemp+1);ACK_ID = input.substring(i1+1);                                                              // ACK ID
          sendATcommand(new GSMNode(cmd, resp, error, 2000), ACK_ID);
        }
        // CONTROL IO PIN (.:IO:<pin number>:<latch delay>:<ACK ID>) //
        // If latch delay is not set, it will toggle the pin state
        // ** RESP: ACK,<ACK ID>
        else if (strstr(&input[0], ".:IO:") != NULL) {
          String          ACK_ID;
          uint8_t         pinNumber;
          unsigned long   latchDelay;
          uint8_t         i1, i2, iTemp;
          byte            contains = false;
          
          i1 = input.indexOf(_delimeter,2);i2 = input.indexOf(_delimeter, i1+1);iTemp = i1;pinNumber = input.substring(i1+1, i2).toInt();     // PIN NUMBER
          i1 = input.indexOf(_delimeter,iTemp+1);i2 = input.indexOf(_delimeter,i1+1);iTemp = i1;
          if (i2 == i1 + 1) latchDelay = -1; else latchDelay = input.substring(i1+1, i2).toInt();                                             // LATCH DELAY
          i1 = input.indexOf(_delimeter,iTemp+1);ACK_ID = input.substring(i1+1);                                                              // ACK_ID
          //digitalWrite(9, !digitalRead(9)); // Toggle alarm
          for (int i = 0; i < sizeof(_reserved_pins)/sizeof(uint8_t); i++)
            if (_reserved_pins[i] == pinNumber) {
              contains = true; break;
            }
          if (!contains) {
            pinMode(pinNumber, OUTPUT); // Ensure the pin is set to OUTPUT
            if (latchDelay == -1) digitalWrite(pinNumber, !digitalRead(pinNumber)); // TOGGLE
            else {                                                                  // LATCH DELAY
              if (!latchToggle) {
                latchStartTime = millis();
                digitalWrite(pinNumber, !digitalRead(pinNumber));
                latchTimeOut = latchDelay;
                latchPin = pinNumber;
                latchToggle = 1;
              } else { Serial.print(F("Pin "));Serial.print(latchPin);Serial.println(F(" is busy")); }
            }
            sendPacket("ACK," + ACK_ID); // Send acknowledgement
          }
        }
        // READ IO PIN (.:IOR:<pin number>:<ACK ID>) //
        // ** RESP: ACK,<state>,<ACK ID>
        else if (strstr(&input[0], ".:IOR:") != NULL) {
          String    ACK_ID, state;
          uint8_t   i1, i2, iTemp, pinNumber;
          i1 = input.indexOf(_delimeter,2);i2 = input.indexOf(_delimeter, i1+1);iTemp = i1;pinNumber = input.substring(i1+1, i2).toInt();     // PIN NUMBER
          i1 = input.indexOf(_delimeter,iTemp+1);ACK_ID = input.substring(i1+1);                                                              // ACK ID
          state = digitalRead(pinNumber);
          sendPacket("ACK," + state + "," + ACK_ID); // Send acknowledgement
        }
        // IS ALIVE (.:IA:<ACK ID>) //
        // ** RESP: ACK,<ACK ID>
        else if (strstr(&input[0], ".:IA:") != NULL) {
          String    ACK_ID;
          uint8_t   i1;
          i1 = input.indexOf(_delimeter,2);ACK_ID = input.substring(i1+1);                                                                     // ACK ID
          sendPacket("ACK," + ACK_ID); // Send acknowledgement
        }*/
        // -------------------
        // -- NODE RESPONSE --
        // -------------------
      } else if (strstr(input, currentNode->cmd) == NULL) { // Ensure it isn't reading back its own CMD
#ifdef __DEBUG__
        int before = currentNode->state;  // Store the state for display purposes
#endif
        if (currentNode != NULL && currentNode->ATSent) currentNode->parseATcommand(input);
#ifdef __DEBUG__
        if (currentNode->state != before) {
          printList();  // For display purposes
        }
#endif
      }
      // RESET INPUT BUFFER //
      memset(input, 0, 50);
    } else {
      strncat(input, &c, 1);
    }
  } // END OF PARSE GSM INPUT (2)
  
  // PROCESS LINKED LIST FROM CURRENT SELECTED NODE //
  if (currentNode != NULL) {
    if (currentNode->state != 1 &&                      // If the node's AT command was OK, move to the next command node.
      (!this->connected() || currentNode->temp) &&      // Ensure that there is a connection by checking the connection status or that the current node is an execution node
      (!currentNode->temp || !currentNode->empty())) {  // Either the node musn't be a temporary node or if it is, then it mustn't be empty
      
      // DESTROY THE EXEC. QUEUE IF THERE IS AN ERROR //
      if (currentNode->temp && (currentNode->state == 2 || currentNode->retryCounter > 2)){
        // This ensures that there are no AT+CIPSEND's without a following packet
        GSMExecutionQueue.destroy(); // Remove all elements in the execution queue
        // This ensures that if there is an error when sending, it will check that it is still connected
        currentNode = GSMStack.head;
        TCP_STATE = TCP_DISCONNECTED;
      }
      
      // ROLL BACK TO PREVIOUS COMMAND //
      else if (currentNode->retryCounter > MAX_NODE_RETRIES ||
              (currentNode->prev == NULL && currentNode->rollBackCounter >= MAX_NODE_ROLLBACK)) { // Ensure that rollbacks onto GSMStack.head (when the rollback counter is exceeded) result in a [hard]reset()
        if (currentNode->prev != NULL) {
          uint8_t _ind = 10; // Failsafe index
          currentNode->reset();

          // Iterate up the GSMStack to find a node which doesn't have a rollback counter which exceeds the limit
          currentNode = currentNode->prev;
#ifdef __DEBUG__
            Serial.print(F("Rolling back to (")); Serial.print(currentNode->cmd); Serial.println(F(")..."));
#endif
          while (currentNode->rollBackCounter >= MAX_NODE_ROLLBACK && --_ind > 0 && currentNode->prev != NULL) {
            currentNode = currentNode->prev;
#ifdef __DEBUG__
            Serial.print(F("Rolling back to (")); Serial.print(currentNode->cmd); Serial.println(F(")..."));
#endif
          }

          // Increment roll back counter //
          currentNode->rollBackCounter++;
          currentNode->reset();
          
        // Handle error with top/GSMStatusNode
        } else {
          // RESET GSM Module
          reset();
        }
      } else {
        if (currentNode->state == 2) currentNode->state = 0; // Reset the state
        currentNode->sendATcommand(GSMPort);
      }
    // HANDLE MOVING ONTO THE NEXT NODE //
    } else {
      if (currentNode->temp) { // A flag to identify if the GSMExecutionQueue is currently being processed
        currentNode = currentNode->next; // Move to the next execution queue node
        // Push the current execution queue node to the back
        GSMExecutionQueue.processHead();
        //if (currentNode->empty()) currentNode = GSMStack.head;
      } else { // NON-EXECUTION NODES
        if (this->connected()) {
          if (GSMExecutionQueue.head != NULL && !GSMExecutionQueue.head->empty()) {
            currentNode = GSMExecutionQueue.head;   // Switch to the execution queue (from GSMStack)
          } else currentNode->reset(); // Reset AT+CIPSTATUS (this won't often happen, as their will usually be items in the execution queue)
        } else if (currentNode->next != NULL) {
          if (currentNode->prev != NULL) // Ensure it isn't the head node
            currentNode->prev->rollBackCounter = 0; // Reset rollback counter (for non execution nodes) for the previous node
          currentNode = currentNode->next;
        }
      }
    }
  } // END OF PROCESSING CURRENT NODE (3)
  
  // PROCESS LATCH //
  if (latchToggle && millis() - latchStartTime >= latchTimeOut) {
    digitalWrite(latchPin, !digitalRead(latchPin));
    latchToggle = false;
  } // END OF PROCESS LATCH (4)

  // PROCESS BATTERY READING // (5)
  if (millis() - lastBatteryRead > batteryReadTimeOut) {
    if (QNF(1)) {
      // Recieve an execution node
      GSMNode * cbc = GSMExecutionQueue.getFree();
      // Create node 
      strcat(cbc->cmd, "AT+CBC"); // CMD      
      strcat(cbc->resp, "CBC"); // Response
      strcat(cbc->error, "ERROR"); // Error      
      cbc->_timeOut = 5000;
      // Set execution flag to true
      cbc->set = true;
    }
    lastBatteryRead = millis();
  }
  
} // END OF __UPDATE_TCP_CONNECTION()__

/*
 * Method used to send packets from the GSMNode
 * -----------------------------------------------------
 * @PARAM pktIn is the packet to be sent
 * @OUTPUT boolean displaying whether or not the packet was successfully appended to the
 *  execution queue.
 */
boolean GSMUtils::sendPacket(char * pktIn) {
  if (QNF(2)) {
    // Receive both the Execution queue GSMNodes
    GSMNode * cipsend = GSMExecutionQueue.getFree();
    GSMNode * packet = cipsend->next;
    // Create packets to be sent
    // Packet
    strcat(packet->cmd, _ID); strcat(packet->cmd, ","); strcat(packet->cmd, pktIn); strcat(packet->cmd, "\n"); // CMD
    strcat(packet->resp, "SEND OK"); // Response
    strcat(packet->error, "SEND FAIL"); // Error
    packet->_timeOut = 1000;
    // Cipsend (Needs to be done second, as the length of 'packet' is required)
    strcat(cipsend->cmd, "AT+CIPSEND=");strcat(cipsend->cmd, String(strlen(packet->cmd)).c_str());
    strcat(cipsend->resp, ">"); // Response
    strcat(cipsend->error, "CONNECT FAIL"); // Error
    cipsend->_timeOut = 1000;
    // Set execution flag to true
    cipsend->set = true;
    packet->set = true;
    
    return true;   
  }
  return false;
}

/*
 * Method used to display the node queue from the currentNode
 */
void GSMUtils::printList() {
#ifdef __DEBUG__
  GSMNode * tmp = currentNode;
  while (tmp != NULL) {
    Serial.print(tmp->cmd); Serial.print(F("(")); Serial.print(tmp->state); Serial.print(F("/")); Serial.print(tmp->rollBackCounter); Serial.print(F(")")); Serial.print(F(" -> "));
    tmp = tmp->next;
  }
  Serial.println(F("(NULL)"));
#endif
}

/*
 * Queue Not Full (QNF)
 * --------------------------------------------------
 * A method to check if there is an available number of spots in the execution queue
 * (Used to check if a packet can be sent (QNF(2))
 * @PARAM amount is the number of required free nodes
 */
boolean GSMUtils::QNF(uint8_t amount) {
  GSMNode   *tmp = GSMExecutionQueue.head;
  uint8_t   slots = 0;
  uint8_t   _index = 10; // Failsafe index
  // First check to see if the tracker is connected
  if (!connected()) return false;
  // proceed...
  while (tmp != NULL && tmp->temp && _index-- > 0) {
    if (tmp->empty()) slots++;
    tmp = tmp->next;
  }
#ifdef __DEBUG__
  Serial.print(F("Exec Queue slots available: "));Serial.println(slots);
#endif
  if (slots >= amount)  return true;
                        return false;
}

/*
 * Method used to do a hard reset on the tracker
 */
void GSMUtils::reset() {
#ifdef __DEBUG__
  Serial.println(F("Resetting GSM"));
#endif
  digitalWrite(RST, LOW);
  delay(500);
  digitalWrite(RST, HIGH);
  // Signal arduino to be reset
  resetArduino = true;
}

void GSMUtils::sendATcommand(GSMNode * cmd, const char * ID) {
  char *tmp = "ACK,"; strcat(tmp, ID);
  if (QNF(2) && sendPacket(tmp)) {
    GSMExecutionQueue.tailAppend(cmd);
  }
}


/* GETTERS / SETTERS */
byte GSMUtils::batteryLevelIsUpdated() { return batteryLevelUpdated; }

char *GSMUtils::getBatteryLevel() { return batteryLevel; }

void GSMUtils::setBatteryLevelUpdated(byte batteryLevelUpdateStatus) { batteryLevelUpdated = batteryLevelUpdateStatus; }

byte GSMUtils::getArduinoReset() { return resetArduino; }

