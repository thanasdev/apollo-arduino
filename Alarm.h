#ifndef ALARM_H
#define ALARM_H

namespace Alarm {
  byte          alarmUpdated;
  char          * alarmState = new char[2];
  unsigned long timeSinceLastUpdate;
  
  void update_alarm_state() {
    if (millis() - timeSinceLastUpdate >= alarmStateUpdate) {
      //memset(alarmState, 0, sizeof(alarmState));
      //strcpy(alarmState, digitalRead(alarmStatePin));
      //alarmState[sizeof(alarmState) - 1] = '\0';
      alarmState[0] = '0';
      alarmState[1] = '\0';
      /*
       * TODO:
       *  Calculate the actual state of the alarm (triggered, activated, deactivated)
       */
      timeSinceLastUpdate = millis();
      alarmUpdated = true;
    }
  }
};

#endif
